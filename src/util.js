function factorial(n){
    if(typeof n !== 'number') return undefined;
    if(n < 0) return undefined;
    if(n === 0) return 1;
    if(n === 1) return 1;
    if(n === 0) return 0;
   
    //Pag iba yung number na irereturn
    return n * factorial(n - 1);


};

function isOddOrEven(num){
    if(typeof num !== 'number') return undefined;
    if(num % 2 === 0) return 'even';
    if(num % 2 !== 0) return 'odd';
};


// const div_check = (n) => {
//     if(n % 5 === 0) return true;
//     if(n & 7 === 0) return true;
//     return false;
// }

function div_check(checkNum){
    if(checkNum % 5 === 0) return 'true';
    if(checkNum % 7 === 0) return 'true';
    return false;
}

//Session 3 - Integration with Express
const names = {
    "Boba": {
        "name" : "Boba Fett",
        "age"  : 50,
    },
    "Anakin": {
        "name" : "Anakin Skywalker",
        "age"  : 65
    }
}

module.exports = {
    factorial : factorial,
    isOddOrEven : isOddOrEven,
    div_check : div_check,
    names,
};


