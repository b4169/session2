/* Session 3 
Routes is use for certain in api */
const { names } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {}})
    })

    app.get('/people', (req, res) => {
        return res.send({
            //Ilalagay sa people property yung mga data na nasa objects names
            people : names
        })
    })

    app.post('/person', (req,res) => {
        //If wala yung parameter ng name
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error' : 'Bad Request - missing required parameter NAME'
            })
        }

        //if yung value is not a string
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error' : 'Bad Request - Name has to be a string'
            })
        }

        //if wala yung parameter na age
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error' : 'Bad Request - missing required parameter AGE'
            })
        }

        //if yung value is not a number
        if(typeof req.body.age !== 'number'){
            return res.status(400).send({
                'error' : 'Bad Request - Name has to be a number'
            })
        }

        if(!req.body.hasOwnProperty('alias')){
            return res.status(400).send({
                'error' : 'Bad Request - missing required parameter ALIS'
            })
        }
    })
}