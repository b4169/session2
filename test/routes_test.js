//Session 3
const chai = require('chai');
const expect = chai.expect;

//same as code in above const {expect} = require('chai')

//to use chai-http need to require
const http = require('chai-http');
//to use the method of chai-http
chai.use(http);

//Test Suites
describe('API Test Suite', () => {
    //Test Cases
    //kaya people kasi nilagay yung names sa people property
    it('Test API GET People are running', () => {
        chai.request('http://localhost:5001').get('/people')
        //.end() is the method that handles the response and error that will be received from the endpoint.
        .end((err, res) => {
            //Dapat may nilalaman yung objects
            expect(res).to.not.equal(undefined);
        });
    });

    it('Test API GET people return 200', (done) => {
        //to check or to test if my irereturn na value
        chai.request('http://localhost:5001')
        .get('/people')
        .end((err,res) => {
            //pag all goods na at running na yung api
            expect(res.status).to.equal(200);
            //mag end ba using done() method
            done();
        });
    });

    //Pag walang nakitang names magrereturn ng 400
    it('Test API POST Person returns 400 if no person name', (done) => {
        chai.request('http://localhost:5001')
        .post('/person')
        .type('json')
        //Pag hindi nakita yung .send, magreretun ng 400
        .send({
            alias : "Jason",
            age   : 28
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        });
    });

    //Session 3 - Activity
    it('[1] Check if person endpoint is running', () => {
        chai.request('http://localhost:5001')
        .post('/people')
        .type('json')
        .send({
            alias : "Dalandan",
            name  : 'Daniel',
            age   : 25
        })
        .end((err, res) => {
            expect(res).to.not.equal(undefined);
        });
    });

    it('[2] Test API post person returns 400 if no ALIAS', (done) => {
        chai.request('http://localhost:5001')
        .post('/people')
        .type('json')
        .send({
            name  : 'Daniel',
            age   : 25
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    });

    it('[3] Test API post person returns 400 if no AGE', (done) => {
        chai.request('http://localhost:5001')
        .post('/people')
        .type('json')
        .send({
            alias : "Dalandan",
            name  : 'Daniel'
        })
        .end((err, res) => {
            expect(res.status).to.not.equal(400);
            done();
        });
    });
});   
