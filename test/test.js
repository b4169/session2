const { factorial, isOddOrEven, div_check } = require('../src/util.js');

const { expect, assert }= require('chai');
const { describe } = require('mocha');

//Test Suites
describe('Test Fun Factorials', () => {
    //Test Case
    //Pag tama yung nireturn na factorial, it will should 120
    it('Test Fun Factorial 5! is 120', () => {
        const product = factorial(5);
        //titignan kung equal ba yung 5 sa 120
        expect(product).to.equal(120);
    });

    it('Test Fun Factorial 1! is 1', () => {
        const product = factorial(1);
        assert.equal(product, 1)
    });

    it('Test Fun Factorial 0! is 1', () => {
        const product = factorial(0);
        assert.equal(product, 1)
    });

    it('Test Fun Factorial 4! is 24', () => {
        const product = factorial(4);
        assert.equal(product, 24)
    });

    it('Test Fun Factorial 10! is 3628800', () => {
        const product = factorial(10);
        expect(product).to.equal(3628800);
    });

    //Session 4 - Test for negative numbers
    it('Test factorial -1 is undefined', () => {
        const product = factorial(-1);
        expect(product).to.equal(undefined);
    })
    
    //Session 4 - Mini-Activity 
    /* this will return error
    it('[1] Test factorial is non-numeric value returns error', () => {
        const product = ('b');
        expect(product).to.equal(undefined);
    }) */

    //Return correct test case
    it('[1] Test factorial is non-numeric value returns error', () => {
        const product = factorial('b');
        expect(product).to.equal(undefined);
    })
});

describe('Test isOddOrEven', () => {
    it('Test isOddOrEven', () => {
        const number = isOddOrEven(2);
        expect(number).to.equal('even');
        //Uses chainable language to construct assertions.
    });

    it('Test is isOddOrEven', ()=> {
        const number = isOddOrEven(5);
        assert.equal(number, 'odd');
        //Uses assert-dot notation that node.js has.
    });

    it('[1] Test isOddOrEven is non-numeric value returns error', () => {
        const product = isOddOrEven('b');
        expect(product).to.equal(undefined);
    })
});

describe('Check Divisibility', () => {
    it('Check number divisible 105 by 5', () => {
        const checkNum = div_check(105);
        assert.equal(checkNum, 'true');
    });

    it('Check number divisible 14 by 7', () => {
        const checkNum = div_check(14);
        assert.equal(checkNum, 'true');
    });

    it('Check number divisible is 0 by 5 OR 7', () => {
        const checkNum = div_check(0);
        assert.equal(checkNum, 'true');
    });

    it('Check number divisible is NOT 22 by 5 OR 7', () => {
        const checkNum = div_check(22);
        assert.notEqual(checkNum, 'false');
    });

    it('[1] Test divisible is non-numeric value returns error', () => {
        const product = div_check('a');
        expect(product).to.equal(false);
    })
});
